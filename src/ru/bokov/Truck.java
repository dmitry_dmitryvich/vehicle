package ru.bokov;

/**
 * Класс, демострирующий подвид транспорта Автомобиль - Грузовой автомобиль
 * @author Bokov Dmitry
 */
public class Truck extends Car {
    private int weight;
    private int run;
    private int mileage;

    /**
     * Метод инициализирует объект класса Truck
     *
     * @param weight грузоподъёмность
     * @param length длина
     * @param width  ширина
     * @param height высота
     * @param run количество рейсов в месяц
     * @param mileage пробег в день
     */

    public Truck (int weight, int length, int width, int height, int run, int mileage){
        super(150, 10, length, width, height);
        this.weight=weight;
        this.run=run;
        this.mileage=mileage;
    }

    public String toString() {
        return "Грузовой автомобиль: " + super.toString() + ", грузоподъёмность: " + this.weight + ", кол-во рейсов в месяц: " + this.run + ", пробег в месяц: " + this.mileage;
    }

    public void Message(){
        super.Message();
        System.out.println("Это сообщение из класса Truck");
    }
}
