package ru.bokov;

/**
 * Класс, демострирующий подвид транспорта Автомобиль - Такси
 * @author Bokov Dmitry
 */
public class Taxi extends Car {
    private int passengers;
    private int mileage;

    /**
     * Метод инициализирует объект класса Taxi
     *
     * @param passengers вместимость
     * @param length длина
     * @param width ширина
     * @param height высота
     * @param mileage пробег в день
     */

    public Taxi(int passengers, int length, int width, int height, int mileage) {
        super(180, 4, length, width, height);
        this.passengers = passengers;
        this.mileage = mileage;
    }

    public String toString() {
        return "Такси: " + super.toString() + ", кол-во мест: " + this.passengers + ", пробег в день " + this.mileage;
    }

    public void Message(){
        super.Message();
        System.out.println("Это сообщение из класса Taxi");
    }
}
