package ru.bokov;

/**
 * Демо-класс для демострации работы Vehicle
 * @author Bokov Dmitry
 */
public class Demo {
    public static void main(String[] args) {

        Airplane plane = new Airplane(1000, 10000, 50, 25, 10, 150);
        System.out.println(plane.toString());
        plane.Message();

        Boat boat = new Boat(250, 40, 20, 15);
        System.out.println(boat.toString());
        boat.Message();

        Car car = new Car(180, 4, 3, 2, 2);
        System.out.println(car.toString());
        car.Message();

        Taxi taxi = new Taxi(3, 5, 3, 2, 120);
        System.out.println(taxi.toString());
        taxi.Message();

        Truck truck = new Truck (4000, 7, 5, 4, 4, 2000);
        System.out.println(truck.toString());
        truck.Message();
    }
}
